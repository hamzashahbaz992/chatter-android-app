package tech.chatter.com.chatter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static tech.chatter.com.chatter.ChatScreen.messageArr;


public class AdapterActivity extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<Chat> data;
    private ArrayList<Chat> filteredData;

    public AdapterActivity(Context context, ArrayList<Chat> m) {
        this.context = context;
        filteredData = m;
        data =m;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int i) {
        return filteredData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = View.inflate(context, R.layout.list, null);
        }
        view.findViewById(R.id.checkBox).setVisibility(View.GONE);
        ImageView im = view.findViewById(R.id.imageView1);
        TextView t = view.findViewById(R.id.textView7);
        TextView d = view.findViewById(R.id.textView4);
        TextView e = view.findViewById(R.id.textView);
        im.setImageBitmap(filteredData.get(i).getImage());
        t.setText(filteredData.get(i).getName());
        try {
            e.setText(messageArr.get(messageArr.size() - 1).getText());
            d.setText(messageArr.get(messageArr.size() - 1).getDate());

        }
        catch (Exception exception)
        {

        }
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it;
                it = new Intent(context, UserDetails.class);
                it.putExtra("number",filteredData.get(i).getNumber());
                context.startActivity(it);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it;
                it = new Intent(context, ChatScreen.class);
                it.putExtra("contactnum",filteredData.get(i).getNumber());
                context.startActivity(it);
            }
        });
        return view;
    }





    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if(constraint != null && constraint.length() > 0){
                    ArrayList<Chat> filteredList = new ArrayList<Chat>();
                    for(int i=0; i < data.size(); i++){
                        if(data.get(i).getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            filteredList.add(data.get(i));
                        }
                    }
                    results.count = filteredList.size();
                    results.values = filteredList;
                }
                else{
                    results.count = data.size();
                    results.values = data;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<Chat>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}