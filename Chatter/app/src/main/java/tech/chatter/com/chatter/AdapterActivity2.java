package tech.chatter.com.chatter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class AdapterActivity2 extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<Contact> data;
    private ArrayList<Contact> filteredData;

    public AdapterActivity2(Context context, ArrayList<Contact> m) {
        this.context = context;
        filteredData = m;
        data =m;
    }
    public void setData(ArrayList<Contact> data) {
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        return (position == 0)? 0: 1;
    }
    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int i) {
        return filteredData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = View.inflate(context, R.layout.list1, null);
        }
        ImageView im = view.findViewById(R.id.imageView2);
        TextView t = view.findViewById(R.id.textView7);
        im.setImageBitmap(filteredData.get(i).getImage());
            t.setText(filteredData.get(i).getName());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent it;
                    it = new Intent(context, ChatScreen.class);
                    it.putExtra("contactnum",filteredData.get(i).getNumber());
                    context.startActivity(it);
                }
            });
        return view;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if(constraint != null && constraint.length() > 0){
                    ArrayList<Contact> filteredList = new ArrayList<Contact>();
                    for(int i=0; i < data.size(); i++){
                        if(data.get(i).getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            filteredList.add(data.get(i));
                        }
                    }
                    results.count = filteredList.size();
                    results.values = filteredList;
                }
                else{
                    results.count = data.size();
                    results.values = data;
                }
                return results;
        }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<Contact>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}