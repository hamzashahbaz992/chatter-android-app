package tech.chatter.com.chatter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


public class AdapterActivity3  extends BaseAdapter implements Filterable{

    private Context context;
    private ArrayList<Message> data;
    private ArrayList<Message> filteredData;
    private FirebaseAuth mAuth;

    public AdapterActivity3(Context context, ArrayList<Message> m) {
        this.context = context;
        filteredData = m;
        data =m;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int i) {
        return filteredData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        if (view == null) {

            v=LayoutInflater.from(context).inflate(R.layout.message,viewGroup,false);
        }


        TextView text2 = v.findViewById(R.id.textView17);
        TextView datetime2 = v.findViewById(R.id.textView18);
        TextView text1 = v.findViewById(R.id.textView14);
        final ImageView im2 = v.findViewById(R.id.imageView7);
        final ImageView im1 = v.findViewById(R.id.imageView6);
        TextView datetime1 = v.findViewById(R.id.textView16);
        mAuth = FirebaseAuth.getInstance();
        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(context,FullScreenImage.class);
                BitmapDrawable drawable = (BitmapDrawable) im2.getDrawable();
                Bitmap bitmap = drawable.getBitmap(); ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                //it.putExtra("BitmapImage", byteArray);
                context.startActivity(it);
            }
        });
        im1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context,FullScreenImage.class);
                        BitmapDrawable drawable = (BitmapDrawable) im1.getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        i.putExtra("BitmapImage", byteArray);
                        context.startActivity(i);
                    }
                });
        final String phone = mAuth.getCurrentUser().getPhoneNumber();
        if(!phone.equals(filteredData.get(i).getSenderID())){
            datetime2.setText("");
            text2.setText("");
            text2.setBackground(null);
            try{
                if(filteredData.get(i).getText().equals("")) {
                    im1.setVisibility(View.VISIBLE);
                    im1.setImageBitmap(data.get(i).getImage());
                }else{
                    im2.setVisibility(View.GONE);
                    im1.setVisibility(View.GONE);
                }
            }catch(Exception e){
                if(filteredData.get(i).getText().equals("")) {
                    im1.setVisibility(View.VISIBLE);
                    im1.setImageResource(R.drawable.pink);
                }
            }
            datetime1.setText("");
            text1.setText(filteredData.get(i).getText());
        }else{
            datetime1.setText("");
            text1.setText("");
            text1.setBackground(null);
            try{
                if(filteredData.get(i).getText().equals("")) {
                    im2.setVisibility(View.VISIBLE);
                    im2.setImageBitmap(data.get(i).getImage());
                }else {
                    im1.setVisibility(View.GONE);
                    im2.setVisibility(View.GONE);
                }
            }catch(Exception e){
                if(filteredData.get(i).getText().equals("")) {
                    im2.setVisibility(View.VISIBLE);
                    im2.setImageResource(R.drawable.pink);
                }
            }
            datetime2.setText("");
            text2.setText(filteredData.get(i).getText());
        }


        return v;
    }

    public void setData(ArrayList<Message> data) {
        this.data = data;
        this.filteredData=data;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null && constraint.length() > 0) {
                    ArrayList<Message> filteredList = new ArrayList<Message>();
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getText().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            filteredList.add(data.get(i));
                        }
                    }
                    results.count = filteredList.size();
                    results.values = filteredList;
                } else {
                    results.count = data.size();
                    results.values = data;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<Message>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}