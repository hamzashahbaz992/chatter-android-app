package tech.chatter.com.chatter;

import android.graphics.Bitmap;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Chat {

    String name;
    Bitmap image;
    String date1;
    String number;

    //String lastText;



    public void setNumber(String number) {
        this.number = number;
    }

    Chat() {
    }

    public Chat(String name, Bitmap image, String date1, String number) {
        this.name = name;
        this.image = image;

        SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String time = localDateFormat.format(new Date());

        this.date1 = time;
        this.number = number;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Bitmap getImage()
    {
        return image;
    }

    public void setImage(Bitmap image)
    {
        this.image = image;
    }

    public String getDate1()
    {
        return date1;
    }



    //this will get string of date and will convert it in format dd/MM/yyyy and store it in local variable
    public void setDate1(String date1)
    {
        SimpleDateFormat localDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dates = localDateFormat.format(new Date(date1));
        this.date1 = dates;
    }
    public String getNumber() {
        return number;
    }




}
