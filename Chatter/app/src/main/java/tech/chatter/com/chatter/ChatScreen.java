package tech.chatter.com.chatter;


import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import static tech.chatter.com.chatter.ChatTab.adapterChat;
import static tech.chatter.com.chatter.ChatTab.chatlist;
import static tech.chatter.com.chatter.MainScreen.contextOfApplication;
import static tech.chatter.com.chatter.MainScreen.finalcontacts;
import static java.security.AccessController.getContext;

//@TargetApi(21)
public class ChatScreen extends AppCompatActivity {

    private View theMenu;
    private boolean menuOpen = false;
    private EditText msgtext;
    private FirebaseAuth mAuth;
    private ListView list;
    static AdapterActivity3 madapter;
    static ArrayList<Message> messageArr = new ArrayList<>();
    static DatabaseReference reference1, reference2;
    DbHelper db;
    FirebaseStorage storage;
    static StorageReference storageRef;
    private boolean show = false;
    String phone;
    static Uri filePath;
    static String picturePath;
    int REQUEST_IMAGE_CAPTURE=268;
    private static final int CAMERA_REQUEST = 1888;

    String id;

    DatabaseReference userref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_screen);

        //initializing local db
        db = new DbHelper(getApplicationContext());


        //getting firebase instance
        mAuth = FirebaseAuth.getInstance();

        //initializing scroll view
        final ScrollView scrollView= (ScrollView)findViewById(R.id.scrollView);

        scrollView.post(new Runnable() {

            @Override
            public void run()
            {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });


        //getting firebase storage instance

        storage=FirebaseStorage.getInstance();

        //getting sender and reciever phone number
        final String phone = getIntent().getStringExtra("contactnum");

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        userref=rootRef.child("Users").child(phone);




        userref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                String namee = phone;

                //this loop is to get name of the user with whom our chat is being displayed
                for(int j=0;j<finalcontacts.size();j++)
                {
                    if (finalcontacts.get(j).getNumber().equals(phone))
                    {
                        namee = finalcontacts.get(j).getName();
                    }
                }

                //now setting name and his online status on action bar...by default we set offline and if it has existence then we get last seen
                getSupportActionBar().setTitle(namee + "  " + "offline");
                try
                {
                    getSupportActionBar().setTitle(namee + "  " + dataSnapshot.child("OnlineStatus").getValue().toString());
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //do nothing
            }
        });


        //getting storage reference and downloading image
        storageRef = storage.getReferenceFromUrl("gs://chatter-947e6.appspot.com/"+phone+".jpg");


        storageRef.child(phone+".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri)
            {
                try
                {
                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    getSupportActionBar().setIcon(Drawable.createFromStream(inputStream, uri.toString()));
                }
                catch (FileNotFoundException e)
                {
                    getSupportActionBar().setIcon(R.drawable.noprofile);
                }
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception exception)
            {
                getSupportActionBar().setIcon(R.drawable.noprofile);
            }
        });

        //Toast.makeText(getApplicationContext(), phone, Toast.LENGTH_LONG).show();

        //getting phone number of current user
        final String phone1=mAuth.getCurrentUser().getPhoneNumber();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //setting the menu invisible because it will be displayed on button click
        theMenu = findViewById(R.id.the_menu);
        theMenu.setVisibility(View.INVISIBLE);

        //getting reference of message box
        msgtext=(EditText) findViewById(R.id.editmsg);

        //getting phone numbers and triming them
        final String phonee = phone.replace("+"," ");
        final String phonee1 = phone1.replace("+"," ");

        Firebase.setAndroidContext(this);


        //getting message from database send from p1 to p2 or vice versa and syncing then for offline use
        reference1=FirebaseDatabase.getInstance().getReference("Messages"+phonee1+phonee);
        reference2=FirebaseDatabase.getInstance().getReference("Messages"+phonee+phonee1);
        reference1.keepSynced(true);
        reference2.keepSynced(true);

        //list = (ListView) findViewById(R.id.chatscreenlist);

        //initialing list view of messages
        madapter = new AdapterActivity3(ChatScreen.this, messageArr);
        //list.setAdapter(madapter);

        reference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {

                adapterChat.notifyDataSetChanged();
                messageArr.clear();
                LinearLayout layout= (LinearLayout) findViewById(R.id.layout1);
                layout.removeAllViews();
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {
                    String message = dsp.child("text").getValue().toString();
                    final String userName = dsp.child("senderID").getValue().toString();
                    String time = dsp.child("date").getValue().toString();
                    String date = dsp.child("time").getValue().toString();
                    String imgUrl=dsp.child("imgURl").getValue().toString();



                    if(imgUrl.equals(""))
                    {
                        if(userName.equals(phone))
                        {
                            //calling function
                            addMessageBox(message,1);
                        }
                        else
                        {
                            //calling function
                            addMessageBox(message, 2);
                        }
                        messageArr.add(new Message(userName, null, time, date, message, imgUrl));
                    }
                    else
                    {
                        messageArr.add(new Message(userName, null, time, date, message, imgUrl));
                        String urll;

                        //adding message to firebase
                        if(!userName.equals(phone))
                        {
                            urll = "gs://chatter-947e6.appspot.com/" + phone + ".jpg/" + imgUrl;
                        }
                        else
                        {
                            urll = "gs://chatter-947e6.appspot.com/" + phone1 + ".jpg/" + imgUrl;
                        }

                        final int index=messageArr.size()-1;
                        Glide.with(getApplicationContext())
                                .using(new FirebaseImageLoader())
                                .load(storage.getReferenceFromUrl(urll))
                                .asBitmap()
                                .into(new SimpleTarget<Bitmap>(100,100) {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {

                                        if(userName.equals(phone))
                                        {
                                            //caaling function
                                            addImage(resource, 1);
                                        }
                                        else
                                        {
                                            //calling function
                                            addImage(resource, 2);
                                        }

                                    }
                                });


                    }
                    if (!db.ifChatExists(phone)) {
                        String name = phone;
                        for (int i = 0; i < finalcontacts.size(); i++)
                        {
                            if (finalcontacts.get(i).getNumber().equals(phone))
                            {
                                name = finalcontacts.get(i).getName();
                            }
                        }
                        Chat c = new Chat(name, null, new Date().toString(), phone);
                        chatlist.add(c);
                        adapterChat.notifyDataSetChanged();
                        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
                        String t = localDateFormat.format(new Date());
                        Bitmap b;
                        db.addChat(phone, BitmapFactory.decodeResource(getResources(), R.drawable.noprofile), new Date().toString(), t);
                    }


                    madapter.notifyDataSetChanged();
                    //list.smoothScrollToPosition(list.getCount() -1);
                    madapter.registerDataSetObserver(new DataSetObserver() {
                        @Override
                        public void onChanged() {
                            super.onChanged();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }

    public void addImage(Bitmap bitmap,int type)
    {
        LinearLayout layout= (LinearLayout) findViewById(R.id.layout1);
        ScrollView scrollView= (ScrollView)findViewById(R.id.scrollView);
        final ImageView imageView=new ImageView(ChatScreen.this);

        Bitmap bitmpa1= Bitmap.createScaledBitmap(bitmap,200,200,true);

        imageView.setImageBitmap(bitmpa1);

        //opening image in full view when clicked
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),FullScreenImage.class);
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();

                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                i.putExtra("BitmapImage", byteArray);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(i);
            }
        });


        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;

        //this decides whether image is from sender side or reciever side
        if(type == 1)
        {
            lp2.gravity = Gravity.LEFT;
        }
        else
        {
            lp2.gravity = Gravity.RIGHT;
        }
        lp2.setMargins(24, 24, 24, 0);
        imageView.setLayoutParams(lp2);
        layout.addView(imageView);

        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    public void addMessageBox(String message, int type){

        LinearLayout layout= (LinearLayout) findViewById(R.id.layout1);
        ScrollView scrollView= (ScrollView)findViewById(R.id.scrollView);
        TextView textView = new TextView(ChatScreen.this);
        textView.setText(message);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;
        lp2.setMargins(24, 24, 24, 0);

        //this decides whether message is from sender side or reciever side
        if(type == 1)
        {
            lp2.gravity = Gravity.LEFT;
            textView.setBackgroundResource(R.drawable.send_msg_layout);
            textView.setTextColor(Color.parseColor("#1e90ff"));
        }
        else
        {
            lp2.gravity = Gravity.RIGHT;
            textView.setBackgroundResource(R.drawable.rcv_msg_layout);
            textView.setTextColor(Color.WHITE);
        }
        textView.setLayoutParams(lp2);
        textView.setPadding(20,10,20,10);
        textView.setTextSize(17);

        layout.addView(textView);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    //code for opening menu
    public void attach(View v)
    {
        //if menu is already opened it hides else it is opened
        if(menuOpen==true)
        {
            menuOpen = false;
            theMenu.setVisibility(View.INVISIBLE);
        }
        else
        {
            menuOpen=true;
            theMenu.setVisibility(View.VISIBLE);
        }
    }

    //code to open gallery
    public void openGallery(View v)
    {

        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, 1);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", "");
        return Uri.parse(path);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 1 || requestCode == CAMERA_REQUEST) && resultCode == RESULT_OK && null != data) {

            Intent intent = new Intent(ChatScreen.this, FilterScreen.class);
            if(requestCode==1)
            {
                filePath = data.getData();
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                intent.setData(selectedImage);

            }
            else if(requestCode == CAMERA_REQUEST)
            {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Uri thumbUri = getImageUri(this, photo);
                intent.setData(thumbUri);
            }

            startActivity(intent);

        }
    }

    public Bitmap getImage(String imgurl) {
        try {
            URL link = new URL(imgurl);
            InputStream in = link.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(in, 1024 * 8);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len = 0;
            byte[] buffer = new byte[1024];
            while ((len = bis.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            out.close();
            bis.close();
            byte[] data = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            return bitmap;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    public void openCamera(View v)
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);


    }



    //code for adding location link
    public void attachLocation(View v)
    {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(ChatScreen.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2688);
            ActivityCompat.requestPermissions(ChatScreen.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2688);

        }

        if (ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            String uri = "http://maps.google.com/maps?saddr=31.48100302,74.30399437";
            try {
                Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double longitude = location.getLongitude();
                double latitude = location.getLatitude();
                uri = "http://maps.google.com/maps?saddr=" + latitude + "," + longitude;
            } catch (Exception e) {

            }
            //   startActivity(new Intent(android.content.Intent.ACTION_VIEW,
            //            Uri.parse(uri)));
            msgtext.setText(uri);
        }

    }

    //code for sending messages
    public void sendMsg(View v)
    {
        String message=msgtext.getText().toString();

        if(!message.equals(""))
        {
            Map<String, String> map = new HashMap<String, String>();
            map.put("message", message);
            map.put("user", mAuth.getCurrentUser().getPhoneNumber());
            msgtext.setText("");

            Date d = new Date();
            SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
            String time = localDateFormat.format(d);

            Message msg = new Message(mAuth.getCurrentUser().getPhoneNumber(), null, time, d.toString(), message,"");

            addMessageBox(message,2);

            messageArr.add(msg);
            madapter.notifyDataSetChanged();

            reference1.push().setValue(msg);
            reference2.push().setValue(msg);
        }
        else
            Toast.makeText(this,"Write Message",Toast.LENGTH_LONG).show();
    }


    @Override
    public void onBackPressed() {

        adapterChat.notifyDataSetChanged();
        if(menuOpen==true) {
            theMenu.setVisibility(View.INVISIBLE);
            menuOpen=false;
        }
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu me)
    {
        getSupportActionBar().setIcon(R.drawable.noprofile);
        getMenuInflater().inflate(R.menu.chatmenu,me);
        SearchView sv = (SearchView) findViewById(R.id.searchView5);
        sv.setVisibility(View.GONE);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem mt)
    {
        int id=mt.getItemId();
        if(id==R.id.op1)
        {
            SearchView sv = (SearchView) findViewById(R.id.searchView5);
            if (show == false) {
                sv.setVisibility(View.VISIBLE);
                sv.setIconified(false);
                show = true;
            }
            else
            {
                sv.setVisibility(View.GONE);
                sv.setIconified(true);
                show = false;
            }

            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    madapter.getFilter().filter(s);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    madapter.getFilter().filter(s);
                    return false;
                }
            });
        }
        else if(id==R.id.op2)
        {
            phone=mAuth.getCurrentUser().getPhoneNumber();
            final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
            DatabaseReference users=rootRef.child("Users");

            users.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    // Result will be holded Here
                    DatabaseReference childs;

                    phone =phone.replace("+"," ");
                    String toDel;

                    for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren()) {
                        String phone2 = dsp.child("number").getValue().toString();
                        phone2=phone2.replace("+"," ");
                        toDel="Messages"+phone+phone2;
                        childs=rootRef.child(toDel);
                        childs.removeValue();
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            messageArr.clear();
            madapter = new AdapterActivity3(ChatScreen.this,messageArr);

        }
        return super.onOptionsItemSelected(mt);
    }

}

