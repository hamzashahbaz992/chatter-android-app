package tech.chatter.com.chatter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static tech.chatter.com.chatter.MainScreen.finalcontacts;
import static java.lang.Thread.sleep;



public class ChatTab extends android.support.v4.app.Fragment {
    private static final String Tag = "ChatTab";
    private boolean show = false;
    ListView list;
    static AdapterActivity adapterChat;
    static ArrayList<Chat> chatlist = new ArrayList<>();
    Context applicationContext = MainScreen.getContextOfApplication();
    DbHelper db;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            //Toast.makeText(applicationContext, intent.getStringExtra("message"), Toast.LENGTH_LONG).show();


            String phone=intent.getStringExtra("phone");
            db = new DbHelper(applicationContext);
            if(!db.ifChatExists(phone)){
                String name=phone;
                for(int i=0;i<finalcontacts.size();i++)
                {
                    if(finalcontacts.get(i).getNumber().equals(phone))
                        name=finalcontacts.get(i).getName();
                }
                Chat c = new Chat(name,null,new Date().toString(),phone);
                chatlist.add(c);
                adapterChat.notifyDataSetChanged();
                SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
                String t = localDateFormat.format(new Date());
                Bitmap b;
                db.addChat(phone, BitmapFactory.decodeResource(getResources(), R.drawable.noprofile),new Date().toString(),t);
            }


        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        // This registers mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(applicationContext)
                .registerReceiver(mMessageReceiver,
                        new IntentFilter("my-integer"));
    }

    @Override
    public void onPause()
    {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(applicationContext)
                .unregisterReceiver(mMessageReceiver);
        super.onPause();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_chat_tab, container, false);
        list = (ListView) v.findViewById(R.id.chatlist);

        db = new DbHelper(applicationContext);

        // if (checkNetwork(applicationContext) != true) {
        //} else {
        chatlist.clear();

        //getting all chats from database
        Cursor c = db.getAllChat();
        while (c.moveToNext())
        {
            Chat chat = new Chat();
            chat.setNumber(c.getString(c.getColumnIndex("Sender")));
            String name = c.getString(c.getColumnIndex("Sender"));
            for (int i = 0; i < finalcontacts.size(); i++) {
                if (finalcontacts.get(i).getNumber().equals(name))
                {
                    name = finalcontacts.get(i).getName();
                }
            }

            //setitng name, image and date of chat
            chat.setName(name);
            chat.setImage(BitmapFactory.decodeByteArray(c.getBlob(c.getColumnIndex("Image")), 0, c.getBlob(c.getColumnIndex("Image")).length));
            String date = c.getString(c.getColumnIndex("Date"));
            chat.setDate1(date);
            chatlist.add(chat);
        }
        c.close();
        //}

        adapterChat = new AdapterActivity(getActivity(), chatlist);
        list.setAdapter(adapterChat);
        final CheckBox cb = v.findViewById(R.id.checkBox);
        list.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                cb.setVisibility(View.VISIBLE);
                return false;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(applicationContext, SelectContact.class);
                startActivity(i);
            }
        });




        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main_screen, menu);
        SearchView sv = (SearchView) getActivity().findViewById(R.id.Searchview);
        sv.setVisibility(View.GONE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                SearchView sv = (SearchView) getActivity().findViewById(R.id.Searchview);
                if (show == false) {
                    sv.setVisibility(View.VISIBLE);
                    sv.setIconified(false);
                    show = true;
                } else {
                    sv.setVisibility(View.GONE);
                    sv.setIconified(true);
                    show = false;
                }

                sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        adapterChat.getFilter().filter(s);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        adapterChat.getFilter().filter(s);
                        return false;
                    }
                });
                break;
            case R.id.refresh:

                for(int k=0;k<chatlist.size();k++)
                {
                    String namee=chatlist.get(k).getNumber();
                    for(int j=0;j<finalcontacts.size();j++)
                    {
                        if(finalcontacts.get(j).getNumber().equals(chatlist.get(k).getNumber())) {
                            namee = finalcontacts.get(j).getName();
                            chatlist.get(k).setName(namee);
                        }
                    }
                }

                adapterChat.notifyDataSetChanged();
                break;
            case R.id.Settings:
                Intent i = new Intent(applicationContext,Settings.class);
                startActivity(i);
                break;
        }
        return true;
    }


    public boolean checkNetwork(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            return true;
        } else
            return false;
    }

}
