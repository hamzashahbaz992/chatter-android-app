package tech.chatter.com.chatter;

import android.graphics.Bitmap;



public class Contact {

    private String name;
    private Bitmap image;
    private String Number;

    Contact()
    {
    }

    public Contact(String name, Bitmap image,String Number)
    {
        this.name = name;
        this.image = image;
        this.Number=Number;

        //this is done to trim all spaces in number string
        this.Number=this.Number.replaceAll("\\s+","");
    }


    @Override
    public boolean equals(Object obj) {

        //this is to check numbers are equal of not
        Contact c=(Contact) obj;

        if(this.Number.equals(c.Number))
            return true;
        else
            return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }


    public String getNumber() {
        this.Number=this.Number.replaceAll("\\s+","");
        return Number;
    }

    public void setNumber(String number) {

        Number = number;
        Number=Number.replaceAll("\\s+","");
    }
}