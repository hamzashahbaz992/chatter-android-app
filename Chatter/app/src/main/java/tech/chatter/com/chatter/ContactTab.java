package tech.chatter.com.chatter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

import static tech.chatter.com.chatter.ChatTab.adapterChat;
import static tech.chatter.com.chatter.ChatTab.chatlist;
import static tech.chatter.com.chatter.MainScreen.finalcontacts;
import static tech.chatter.com.chatter.MainScreen.finallist;
import static tech.chatter.com.chatter.SelectContact.adapterselectcontact;

public class ContactTab extends android.support.v4.app.Fragment {
    private static final String Tag = "ContactTab";
    private boolean show = false;
    static ListView list;
    StorageReference storageRef;
    FirebaseStorage storage;
    static AdapterActivity1 adapter;
    static ArrayList<Contact> contactlist = new ArrayList<>();
    private Cursor crContacts;
    Context applicationContext = MainScreen.getContextOfApplication();
    DbHelper db;
    int j;
    boolean carryon=true;
    boolean once=true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_contact_tab,container,false);
        list = (ListView)v.findViewById(R.id.contactlist);

        final ArrayList l2 = new ArrayList();

        storage=FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl(("gs://chatter-947e6.appspot.com/"));

        Iterator iterator = contactlist.iterator();

        while (iterator.hasNext())
        {
            Contact o = (Contact) iterator.next();
            if(!l2.contains(o))
            {
                l2.add(o);

            }
        }

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users=rootRef.child("Users");



        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Result will be holded Here

                for (DataSnapshot dsp : dataSnapshot.getChildren()) {

                    String phonenum = dsp.child("number").getValue().toString();
                    //String userName = dsp.child("status").getValue().toString();
                    finallist.add(phonenum); //add result into array list
                }

                for(int i=0;i<l2.size();i++)
                {
                    Contact c=(Contact) l2.get(i);
                    if (c.getNumber().equals("03044778755"))
                    {
                        c.setNumber("+923044778755");
                    }
                    if(finallist.contains(c.getNumber())) {

                        finalcontacts.add(c);
                    }
                }
                adapter = new AdapterActivity1(applicationContext,finalcontacts);
                list.setAdapter(adapter);
                //setimages();
                if(once) {
                    once=false;
                    new ImageDownload().execute("");
                }

                for(int k=0;k<chatlist.size();k++)
                {
                    String namee=chatlist.get(k).getNumber();
                    for(int j=0;j<finalcontacts.size();j++)
                    {
                        if(finalcontacts.get(j).getNumber().equals(chatlist.get(k).getNumber())) {
                            namee = finalcontacts.get(j).getName();
                            chatlist.get(k).setName(namee);
                        }
                    }
                }

                adapterChat.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        db = new DbHelper(applicationContext);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab2);
        fab.bringToFront();
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                view.getContext().startActivity(intent);

            }
        });




        return v;
    }


   /* public void setimages()
    {
        int f=0;
        int k=-1;
        for(j =0;j<finalcontacts.size();) {
            if(k!=j) {
                f++;
                k=j;
                storageRef.child(finalcontacts.get(j).getNumber() + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        ImageRequest request = new ImageRequest(uri.toString(),
                                new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(Bitmap bitmap) {
                                        finalcontacts.get(j).setImage(bitmap);
                                        adapter.setData(finalcontacts);
                                        adapter.notifyDataSetChanged();
                                    }
                                }, 0, 0, null,
                                new Response.ErrorListener() {
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                });
                        ++j;
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                        Toast.makeText(applicationContext, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }


//
    }

   /* public Bitmap getImage(String imgurl) {
        try {
            URL link = new URL(imgurl);
            InputStream in = link.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(in, 1024 * 8);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len = 0;
            byte[] buffer = new byte[1024];
            while ((len = bis.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            out.close();
            bis.close();
            byte[] data = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            return bitmap;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean checkNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            return true;
        }
        else
            return false;
    }
    */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.menu_main_screen, menu);
        SearchView sv =(SearchView)getActivity().findViewById(R.id.sv2);
        sv.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.search:
                SearchView sv = (SearchView)getActivity().findViewById(R.id.sv2);
                if(show == false){
                    sv.setVisibility(View.VISIBLE);
                    sv.setIconified(false);
                    show =true;
                }
                else{
                    sv.setVisibility(View.GONE);
                    sv.setIconified(true);
                    show =false;
                }

                sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        adapter.getFilter().filter(s);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        adapter.getFilter().filter(s);
                        return false;
                    }
                });
                break;
            case R.id.Settings:
                Intent i = new Intent(applicationContext,Settings.class);
                startActivity(i);
                break;
        }
        return true;
    }

    private class ImageDownload extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... param) {

            int k=-1;


            try {
                for (j = 0; j < finalcontacts.size() && carryon; ) {
                    if (k != j) {
                        k = j;
                        final long ONE_MEGABYTE = 1024 * 1024;
                        storageRef.child(finalcontacts.get(j).getNumber() + ".jpg")
                                .getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                finalcontacts.get(j).setImage(bitmap);

                                for(int m=0;m<chatlist.size();m++) {
                                    if(chatlist.get(m).getNumber().equals(finalcontacts.get(j).getNumber()))
                                    {
                                        chatlist.get(m).setImage(bitmap);
                                        adapterChat.notifyDataSetChanged();
                                    }
                                }
                                //  adapter.setData(finalcontacts);
                                adapter.notifyDataSetChanged();
                                j++;
                                if(j==finalcontacts.size())
                                    carryon=false;


                                // Data for "images/island.jpg" is returns, use this as needed
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                            }
                        });
                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Void result) {
            adapter.setData(finalcontacts);
            adapter.notifyDataSetChanged();
        }
    }
}



