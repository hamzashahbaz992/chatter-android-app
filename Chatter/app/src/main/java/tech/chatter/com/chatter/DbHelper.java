package tech.chatter.com.chatter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;

import java.io.ByteArrayOutputStream;


public class DbHelper extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Chatter.db";
    final int THUMBSIZE = 64;

    public DbHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db)
    {
        String sql = "CREATE TABLE Contacts (Id TEXT PRIMARY KEY, " +
                "Name TEXT," +
                "Image BLOB)";
        db.execSQL(sql);
        sql = "CREATE TABLE MyDetails (Id TEXT PRIMARY KEY, " +"Name TEXT," +
                "Status TEXT," +
                "Image BLOB)";
        db.execSQL(sql);
        sql = "CREATE TABLE Chats (Sender TEXT PRIMARY KEY, "+
                "Image BLOB, "+
                "Date TEXT, "+
                "Time TEXT)";
        db.execSQL(sql);
        sql = "CREATE TABLE ChatScreen (Id TEXT PRIMARY KEY, " +
                "SenderID TEXT," +
                "Chat TEXT,"+
                "Image BLOB,"+
                "Date TEXT,"+
                "Time TEXT)";
        db.execSQL(sql);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS Contacts");
        db.execSQL("DROP TABLE IF EXISTS Chats");
        db.execSQL("DROP TABLE IF EXISTS ChatScreen");
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db,oldVersion,newVersion);
    }


    //function to add contact to database table
    public boolean addContact(String i, String Name1, Bitmap p)
    {
        SQLiteDatabase db = getWritableDatabase();

        //converting the image in thumbnail size
        Bitmap ThumbImage= ThumbnailUtils.extractThumbnail(p,THUMBSIZE,THUMBSIZE);

        //creating an array of 32bytes and compressing size of image
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ThumbImage.compress(Bitmap.CompressFormat.PNG, 0, stream);


        //inserting values in database
        ContentValues contentValues = new ContentValues();
        contentValues.put("Id", i);
        contentValues.put("Name", Name1);
        contentValues.put("Image", stream.toByteArray());
        db.insert("Contacts",null,contentValues);
        return true;
    }

    //function to get all contacts from database against the of particular user
    public Cursor getContact(String id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM Contacts where Id = '"+id+"'";
        Cursor res = db.rawQuery(Query, null);
        return res;
    }




    //inserting user chats in database
    public boolean addChat(String Name1, Bitmap p,String date,String time)
    {
        SQLiteDatabase db = getWritableDatabase();

        //converting image in thumnail format
        Bitmap ThumbImage= ThumbnailUtils.extractThumbnail(p,THUMBSIZE,THUMBSIZE);

        //compressing image
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ThumbImage.compress(Bitmap.CompressFormat.PNG, 0, stream);


        ContentValues contentValues = new ContentValues();
        contentValues.put("Sender", Name1);
        contentValues.put("Image", stream.toByteArray());
        contentValues.put("Date", date);
        contentValues.put("Time", time);
        db.insert("Chats",null,contentValues);
        return true;
    }



    public Cursor getChat(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM Chats where Id = '"+Integer.toString(id)+"'";
        Cursor res = db.rawQuery(Query, null);
        return res;
    }

    public Cursor getAllChat() {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM Chats";
        Cursor res = db.rawQuery(Query, null);
        return res;
    }

    //this function checks whether there are any chats or not against a given phone number
    public boolean ifChatExists(String phone) {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM Chats where Sender='"+phone+"'";
        Cursor res = db.rawQuery(Query, null);
        while(res.moveToNext())
        {
            return true;
        }
        return false;
    }

    public boolean addChatScreen(int i, int senderID, Bitmap p,String text,String date,String time)
    {
        SQLiteDatabase db = getWritableDatabase();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        p.compress(Bitmap.CompressFormat.PNG, 0, stream);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Id", Integer.toString(i));
        contentValues.put("SenderID", senderID);
        contentValues.put("Chat",text);
        contentValues.put("Image", stream.toByteArray());
        contentValues.put("Date", date);
        contentValues.put("Time", time);
        db.insert("ChatScreen",null,contentValues);
        return true;
    }

    public Cursor getChatScreen(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM ChatScreen where Id = '"+Integer.toString(id)+"'";
        Cursor res = db.rawQuery(Query, null);
        return res;
    }

    public boolean addMyDetails(String n, Bitmap p) {
        SQLiteDatabase db = getWritableDatabase();
        Bitmap ThumbImage= ThumbnailUtils.extractThumbnail(p,THUMBSIZE,THUMBSIZE);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ThumbImage.compress(Bitmap.CompressFormat.PNG, 0, stream);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Id", 1);
        contentValues.put("Name", n);
        contentValues.put("Status", "Hello! I'm using Chatter");
        contentValues.put("Image", stream.toByteArray());
        db.insert("MyDetails",null,contentValues);
        return true;
    }

    //will be called when user deletes account
    public boolean deleteTable()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from "+ "MyDetails");
        return true;
    }

    //functions to update name,status and profile picture

    public boolean updateMyPicture(Bitmap p) {
        SQLiteDatabase db = getWritableDatabase();
        Bitmap ThumbImage= ThumbnailUtils.extractThumbnail(p,THUMBSIZE,THUMBSIZE);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ThumbImage.compress(Bitmap.CompressFormat.PNG, 0, stream);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Image", stream.toByteArray());
        db.update("MyDetails",contentValues,"Id = 1",null);
        return true;
    }

    public boolean updateMyStatus(String Status) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Status", Status);
        db.update("MyDetails",contentValues,"Id = 1",null);
        return true;
    }



    public boolean updateMyName(String n) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", n);
        db.update("MyDetails",contentValues,"Id = 1",null);
        return true;
    }


    //checks whether there are any details of that person or not
    public boolean getMyDetails()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM MyDetails";
        Cursor res = db.rawQuery(Query, null);
        while(res.moveToNext())
        {
            return true;
        }
        return false;
    }

    //returns details of a person
    public Cursor showMyDetails() {
        SQLiteDatabase db = this.getReadableDatabase();
        String Query ="SELECT * FROM MyDetails";
        Cursor res = db.rawQuery(Query, null);
        return res;
    }





}
