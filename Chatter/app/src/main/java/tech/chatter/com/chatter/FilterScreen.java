package tech.chatter.com.chatter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static tech.chatter.com.chatter.ChatScreen.filePath;
import static tech.chatter.com.chatter.ChatScreen.madapter;
import static tech.chatter.com.chatter.ChatScreen.messageArr;
import static tech.chatter.com.chatter.ChatScreen.picturePath;
import static tech.chatter.com.chatter.ChatScreen.reference1;
import static tech.chatter.com.chatter.ChatScreen.reference2;
import static tech.chatter.com.chatter.ChatScreen.storageRef;
import static tech.chatter.com.chatter.ContactTab.adapter;
import android.provider.MediaStore;
public class FilterScreen extends AppCompatActivity {

    ImageView imageview;
    ImageView imageview1;
    ImageView filterview;
    Bitmap bit;
    ImageButton imageButton;


    FirebaseStorage storage;
    private FirebaseAuth mAuth;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_screen);

        imageButton=(ImageButton)findViewById(R.id.imageButton);

        Uri imageUri = getIntent().getData();
        Bitmap bmp = null;
        try {
            bmp = getBitmapFromUri(imageUri);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        imageview = (ImageView) findViewById(R.id.imgView);
        imageview1 = (ImageView) findViewById(R.id.imgView);
        filterview = (ImageView) findViewById(R.id.filterView);
        filterview.setAlpha(100);
        imageview.setImageBitmap(bmp);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    //function to combine 2 images to 1
    public Bitmap combineImages(Bitmap c, Bitmap s)
    { // can add a 3rd parameter 'String loc' if you want to save the new image - left some code to do that at the bottom
        Bitmap bmOverlay = Bitmap.createBitmap(c.getWidth(), c.getHeight(), c.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(c, new Matrix(), null);
        Paint transparentpainthack = new Paint();
        transparentpainthack.setAlpha(80);
        canvas.drawBitmap(s, 0, 0, transparentpainthack);
        return bmOverlay;
    }



    public void ApplyFilter(View view)
    {
        filterview.setImageResource(R.drawable.yellowblur);
        Bitmap b = combineImages(((BitmapDrawable)imageview.getDrawable()).getBitmap(),((BitmapDrawable)filterview.getDrawable()).getBitmap());
        imageview.setImageBitmap(b);
        filterview.setVisibility(View.INVISIBLE);
    }

    public void ApplyFilter1(View view)
    {
        filterview.setImageResource(R.drawable.green);
        Bitmap b = combineImages(((BitmapDrawable)imageview.getDrawable()).getBitmap(),((BitmapDrawable)filterview.getDrawable()).getBitmap());
        imageview.setImageBitmap(b);
        filterview.setVisibility(View.INVISIBLE);
    }

    public void ApplyFilter2(View view)
    {
        filterview.setImageResource(R.drawable.blue);
        Bitmap b = combineImages(((BitmapDrawable)imageview.getDrawable()).getBitmap(),((BitmapDrawable)filterview.getDrawable()).getBitmap());
        imageview.setImageBitmap(b);
        filterview.setVisibility(View.INVISIBLE);
    }

    public void ApplyFilter3(View view)
    {
        filterview.setImageResource(R.drawable.pink);
        Bitmap b = combineImages(((BitmapDrawable)imageview.getDrawable()).getBitmap(),((BitmapDrawable)filterview.getDrawable()).getBitmap());
        imageview.setImageBitmap(b);
        filterview.setVisibility(View.INVISIBLE);
    }

    public void sendImage(View view)
    {
        final Bitmap bitmap=((BitmapDrawable)imageview.getDrawable()).getBitmap();

        String id1 =String.valueOf(UUID.randomUUID());
        ///save image in gallery
        String savedImageURL = MediaStore.Images.Media.insertImage(
                getContentResolver(),
                bitmap,
                id1,
                "Chatter image"
        );
        Uri savedImageURI = Uri.parse(savedImageURL);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        //not showing button when image is being sent
        imageButton.setVisibility(View.GONE);

        //getting firebase instance
        mAuth = FirebaseAuth.getInstance();

        //generating a random id/filename
        id =String.valueOf(UUID.randomUUID());

        //storing the image
        StorageReference childRef = storageRef.child(id + ".jpg");

        //uploading image
        UploadTask uploadTask = childRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Toast.makeText(getApplicationContext(), "Upload Failed -> " + exception, Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.

                //setting date of image when sent
                Date d = new Date();
                SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
                String time = localDateFormat.format(d);
                Message msg1 = new Message(mAuth.getCurrentUser().getPhoneNumber(), null, time, d.toString(), "", id + ".jpg");

                reference1.push().setValue(msg1);
                reference2.push().setValue(msg1);

                msg1.setImage(bitmap);
                messageArr.add(msg1);
                madapter.setData(messageArr);
                madapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Updated Picture Successfully!", Toast.LENGTH_LONG).show();
                onBackPressed();

            }
        });
    }

}
