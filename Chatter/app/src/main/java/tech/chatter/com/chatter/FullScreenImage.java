package tech.chatter.com.chatter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class FullScreenImage extends Activity {
    ImageView Im;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        //getting byteArray passed from previous screen
        Intent intent = getIntent();
        byte[] b = getIntent().getExtras().getByteArray("BitmapImage");
        Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);

        //initializing imagebox
        Im=(ImageView)findViewById(R.id.imageView);

        //setting image
        Im.setImageBitmap(bitmap);

        //when image is clicked it goes back to setting screen
       /* Im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(),Settings.class);
                startActivity(intent);
                finish();
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
