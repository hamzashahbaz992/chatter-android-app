package tech.chatter.com.chatter;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static tech.chatter.com.chatter.ContactTab.contactlist;

public class MainScreen extends AppCompatActivity{

    private static final String Tag = "MainActivity";
    private SectionsPageAdapter _SectionsPageAdapter;
    public static Context contextOfApplication;
    private ViewPager vp;
    private Cursor crContacts;
    static ArrayList<Contact> finalcontacts;
    static ArrayList<String> finallist;
    ListView list;
    AdapterActivity1 adapter;
    private DatabaseReference userRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        //initializing firebase
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users=rootRef.child("Users");



        FirebaseAuth mAuth=FirebaseAuth.getInstance();

        //getting current user number
        userRef=users.child(mAuth.getCurrentUser().getPhoneNumber());


        //starting message service
        startService(new Intent(this,MessageService.class));

        //calling function
        loadContacts();

        contextOfApplication = getApplicationContext();
        _SectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());
        vp = (ViewPager) findViewById(R.id.container);
        setViewPage(vp);
        TabLayout TL = (TabLayout) findViewById(R.id.tabs);
        TL.setupWithViewPager(vp);
    }

    //function to load contacts
    public void loadContacts()
    {
        crContacts = ContactHelper.getContactCursor(this.getContentResolver(), "");
        crContacts.moveToFirst();

        Bitmap img = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_person_pin_circle_white_24dp);

        while (!crContacts.isAfterLast())
        {
            contactlist.add(new Contact(crContacts.getString(1),img,crContacts.getString(2)));
            crContacts.moveToNext();
        }

        finallist=new ArrayList<>();
        finalcontacts=new ArrayList<>();
    }

    //making tabs
    private void setViewPage(ViewPager v){
        final SectionsPageAdapter adapt = new SectionsPageAdapter(getSupportFragmentManager());

        //initializing chattab
        adapt.addFragment(new ChatTab(),"Chats");

        //initializing contact tab
        adapt.addFragment(new ContactTab(),"Contacts");
        v.setAdapter(adapt);
    }
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    //changing online status of user in firebase
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        userRef.child("OnlineStatus").setValue("Online");

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        userRef.child("OnlineStatus").setValue("Online");

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        userRef.child("OnlineStatus").setValue("Online");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Date d=new Date();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String time=dateFormat.format(d);

        userRef.child("OnlineStatus").setValue(time);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        userRef.child("OnlineStatus").setValue("Online");

    }

    @Override
    protected void onStop()
    {
        super.onStop();

        Date d=new Date();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String time=dateFormat.format(d);

        userRef.child("OnlineStatus").setValue("Online");

    }
}
