package tech.chatter.com.chatter;

import android.graphics.Bitmap;



public class Message {
    private String senderID;
    private Bitmap Image;
    private String time;
    private String date;
    private String text;
    private String imgURl;

    public Message(String senderID, Bitmap image, String time, String date, String text, String imgUrl) {
        this.senderID = senderID;
        Image = image;
        this.time = time;
        this.date = date;
        this.text = text;
        this.imgURl=imgUrl;
    }


    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgURl() {
        return imgURl;
    }

    public void setImgURl(String imgUrl) {
        this.imgURl = imgUrl;
    }
}