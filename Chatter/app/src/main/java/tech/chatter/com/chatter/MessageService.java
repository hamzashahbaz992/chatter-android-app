package tech.chatter.com.chatter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

public class MessageService extends Service {

    private Firebase reference1, reference2;
    private FirebaseAuth mAuth;
    private ArrayList<String> regusers;

    public MessageService() {
        try
        {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            FirebaseDatabase.getInstance().getReference("Users");
        }
        catch (Exception ex)
        {

        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    public void getRegUsers()
    {
        regusers=new ArrayList<>();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users=rootRef.child("Users");
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot)
            {
                // Result will be holded Here
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {
                    //regusers.add(String.valueOf(dsp.getValue())); //add result into array list
                    try
                    {
                        String phonenum = dsp.child("number").getValue().toString();
                        regusers.add(phonenum); //add result into array list
                    }
                    catch(Exception e)
                    {

                    }
                }
                addListeners();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //do nothing
            }
        });
    }

    public void addListeners()
    {
        mAuth = FirebaseAuth.getInstance();
        final String phone = mAuth.getCurrentUser().getPhoneNumber();

        Firebase.setAndroidContext(this);

        for(int j=0;j<regusers.size();j++) {
            reference1 = new Firebase("https://chatter-947e6.firebaseio.com/Messages" + phone + regusers.get(j));
            reference2 = new Firebase("https://chatter-947e6.firebaseio.com/Messages" + regusers.get(j) + phone);

            reference1.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Map map = dataSnapshot.getValue(Map.class);
                    String message = map.get("text").toString();
                    String userName = map.get("senderID").toString();
                    String time = map.get("date").toString();
                    String date = map.get("time").toString();



                    if(!userName.equals(phone)) {
                        notifyy(message);
                        Intent intent = new Intent("my-integer");
                        // Adding some data
                        intent.putExtra("phone", userName);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        getRegUsers();
    }

    public void notifyy(String Message)
    {
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("RSSPullService");

        Intent myintent=new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        PendingIntent pendingIntent=PendingIntent.getActivity(getBaseContext(),0,myintent,PendingIntent.FLAG_ONE_SHOT);

        Context context=getApplicationContext();

        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSound(uri)
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                        .setContentTitle("Chatter")
                        .setContentText(Message);

        NotificationManager mNotificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }
}
