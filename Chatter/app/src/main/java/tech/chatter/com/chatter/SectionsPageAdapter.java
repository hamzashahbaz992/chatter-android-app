package tech.chatter.com.chatter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;



public class SectionsPageAdapter extends FragmentPagerAdapter {
    private final List<Fragment> FL = new ArrayList<>();
    private final List<String> FLTitle = new ArrayList<>();

    public SectionsPageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment f,String t){
        FL.add(f);
        FLTitle.add(t);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return FLTitle.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return FL.get(position);
    }

    @Override
    public int getCount() {
        return FL.size();
    }
}
