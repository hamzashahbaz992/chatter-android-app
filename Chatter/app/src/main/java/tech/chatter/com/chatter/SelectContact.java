package tech.chatter.com.chatter;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import static tech.chatter.com.chatter.MainScreen.finalcontacts;
import static tech.chatter.com.chatter.R.id.container;

public class SelectContact extends AppCompatActivity {
    ListView list;
    static AdapterActivity2 adapterselectcontact;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_contact);
        this.getSupportActionBar().setTitle("Select Contact");


        //loading contacts
        list = (ListView)findViewById(R.id.selectcontact);

        adapterselectcontact = new AdapterActivity2(this,finalcontacts);
        list.setAdapter(adapterselectcontact);
    }

}
