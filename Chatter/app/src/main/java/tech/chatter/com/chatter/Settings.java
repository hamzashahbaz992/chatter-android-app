package tech.chatter.com.chatter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Settings extends AppCompatActivity {
    private boolean zoomOut =  false;
    ImageView Im;
    DbHelper db;
    EditText status;
    String text1;
    EditText Name1;
    String text;
    Uri filePath;
    ProgressDialog pd;
    StorageReference storageRef;
    FirebaseStorage storage;
    private FirebaseAuth mAuth;
    String picturePath;
    String phone;

    int RESULT_LOAD_IMAGE=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //initializing status,image and name textbox
        status = (EditText)findViewById(R.id.textView10);
        Im = (ImageView) findViewById(R.id.imageView2);
        Name1= (EditText)findViewById(R.id.textView9);

        //initializing firebase storage
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://chatter-947e6.appspot.com/");

        //getting firebase instance
        mAuth = FirebaseAuth.getInstance();

        //getting phone number of user
        String name = mAuth.getCurrentUser().getPhoneNumber();
        phone=mAuth.getCurrentUser().getPhoneNumber();
        name = name.substring(1);

        //initialing dialogue box
        pd = new ProgressDialog(Settings.this);
        pd.setMessage("Uploading Image...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        //initializing local database
        db = new DbHelper(this);

        //when status is change
        status.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                {
                    //this checks whether any changes are made in status or not
                    if(!text.equals(status.getText().toString()))
                    {
                        //updates status in local database
                        db.updateMyStatus((String)status.getText().toString());
                        text = status.getText().toString();

                        //call function addstatusfirebase to change status in firebase
                        addstatusFirebase(text);
                        Toast.makeText(getApplicationContext(), "Status Changed Successfully!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        //when name is changed
        Name1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                {
                    //check whether changes are made in name or not
                    if(!text1.equals(Name1.getText().toString()))
                    {
                        //make changes in local database
                        db.updateMyName((String)Name1.getText().toString());
                        text1 = Name1.getText().toString();
                        Toast.makeText(getApplicationContext(), "Name Changed Successfully!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        //when image in clciked just once it open it in full screen
        Im.setOnClickListener(new ImageView.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                //if opens new screen of fullScreenImage and send images by converting it to byteArray
                Intent i = new Intent(getApplicationContext(),FullScreenImage.class);
                BitmapDrawable drawable = (BitmapDrawable) Im.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                i.putExtra("BitmapImage", byteArray);
                startActivity(i);
            }
        });

        //when image is presses for long time it opens gallery
        Im.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                //open gallery
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
                return false;
            }
        });

        //loading default values in textboxes
        boolean limit = true;
        while(limit)
        {
            Cursor c =db.showMyDetails();
            if (c.moveToFirst())
            {
                //clonning name and status
                text1 = c.getString(c.getColumnIndex("Name"));
                text = c.getString(c.getColumnIndex("Status"));

                //seeting status, name and loading image
                status.setText(c.getString(c.getColumnIndex("Status")));
                Name1.setText(c.getString(c.getColumnIndex("Name")));
                Im.setImageBitmap(BitmapFactory.decodeByteArray(c.getBlob(c.getColumnIndex("Image")), 0, c.getBlob(c.getColumnIndex("Image")).length));
                c.close();
                limit = false;
            }
            else
                limit = false;
        }
    }

    public void addstatusFirebase(final String st)
    {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users=rootRef.child("Users");
        users.addListenerForSingleValueEvent(new ValueEventListener()
        {
            //when changes are made this function is called to make changes in firebase
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot)
            {
                // Result will be holded Here
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {
                    //getting phone number 1 by 1 from Users database in firebase
                    String phonenum = dsp.child("number").getValue().toString();

                    //when matches with our number
                    if(phonenum.equals(mAuth.getCurrentUser().getPhoneNumber()))
                    {
                        //changes status
                        dsp.child("status").getRef().setValue(st);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //do nothing
            }
        });
    }

    //code for uploading image
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //checks whther images is loaded or not
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            filePath = data.getData();
            try
            {
                Uri selectedImage = filePath;
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                pd.show();

                //if image is loaded
                if (filePath != null)
                {
                    //gets firebase instance
                    mAuth = FirebaseAuth.getInstance();

                    //gets user phone number
                    String name = mAuth.getCurrentUser().getPhoneNumber();

                    //changes name to pic number.jpg to store in firebase storage
                    StorageReference childRef = storageRef.child(name + ".jpg");

                    //uploading image
                    UploadTask uploadTask = childRef.putFile(filePath);

                    //success case
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
                    {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                        {
                            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                            Bitmap ThumbImage= ThumbnailUtils.extractThumbnail(bitmap,64,64);

                            //when image is succesfully uploaded
                            if(db.updateMyPicture(ThumbImage))
                            {
                                pd.dismiss();  //dialogue box is dismissed
                                Toast.makeText(getApplicationContext(), "Updated Picture Successfully!", Toast.LENGTH_LONG).show();

                                //new image is shown in image box
                                Im.setImageBitmap(ThumbImage);
                            }
                            pd.dismiss();
                        }
                    }).addOnFailureListener(new OnFailureListener()  //failure case
                    {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), "Upload Failed -> " + e, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (requestCode == 234)
        {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    //Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            View v = getCurrentFocus();
            if ( v instanceof EditText)
            {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    //code for inviting friend
    public void inviteFriend(View view)
    {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                .build();
        startActivityForResult(intent, 234);
    }

    //it will be used in deleteAcc function
    public void deleteData()
    {
        final ArrayList<String> regusers=new ArrayList<>();

        //getting firebase of Users
        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference users=rootRef.child("Users");

        //removing from firebase
        users.child(phone).removeValue();

        //initialing local database and deleting table
        DbHelper db;
        db = new DbHelper(this);
        db.deleteTable();


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                {
                }
                else
                {
                }
            }
        });


        FirebaseAuth.getInstance().signOut();

        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                // Result will be holded Here
                DatabaseReference childs;

                phone =phone.replace("+"," ");
                String toDel;

                //deelting messages
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {
                    String phone2 = dsp.child("number").getValue().toString();
                    phone2=phone2.replace("+"," ");
                    toDel="Messages"+phone+phone2;
                    childs=rootRef.child(toDel);
                    childs.removeValue();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //do nothing
            }
        });
    }

    //code for deleting account
    public void deleteAcc(View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Do you really want to delete your account?");

        //code when Yes button is pressed
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                //calling above function
                deleteData();

                //when account is deleted it agains open user registration screen
                Intent I = new Intent(getApplicationContext(),UserRegistration.class);
                startActivity(I);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
