package tech.chatter.com.chatter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.Manifest;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class StartingDetails extends AppCompatActivity {

    private ImageButton Im;
    private EditText Status;
    DbHelper db;
    Uri filePath;
    ProgressDialog pd;
    StorageReference storageRef;
    FirebaseStorage storage;
    private FirebaseAuth mAuth;

    int RESULT_LOAD_IMAGE = 1;
    private static final int PICK_FROM_GALLERY = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_details);

        //setting storage and adding storage reference
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://chatter-947e6.appspot.com/");


        //getting image button and setting default tag to abc
        Im = (ImageButton) this.findViewById(R.id.imageButton3);
        Im.setTag("abc");

        //getting ref of next button
        Button next = (Button) this.findViewById(R.id.button2);

        //getting ref of status textbox
        Status = (EditText) this.findViewById(R.id.editText);


        //initializing local database
        db = new DbHelper(this);

        //this check whether user already exists or not if exist if redirects to main screen
        if(db.getMyDetails()) {
            Intent i = new Intent(getApplicationContext(),MainScreen.class);
            startActivity(i);
        }


        //this show dialog box while image is being uploaded to storage
        pd = new ProgressDialog(StartingDetails.this);
        pd.setMessage("Uploading Image...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);

        //when image button is clicked it asks for permission and opens gallery
        Im.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(StartingDetails.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions(StartingDetails.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                }
                else
                {
                    //code to open gallery

                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }


            }
        });

        //when next button is clicked
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //it first check whether network is available or not
                if(checkNetwork(v.getContext()))
                {

                    //it check whether name is empty or not
                    if (Status.getText().toString().equals(""))
                    {
                        Toast.makeText(StartingDetails.this, "Enter Name", Toast.LENGTH_SHORT).show();
                    }
                    //it checks wheter an image is selected or not .... by default tag is abc if its still abc means its not selected
                    if (!"abc".equals((String) Im.getTag()) && (!Status.getText().toString().equals("")))
                    {

                        //if image is selected
                        if (filePath != null)
                        {
                            //pd.show shows dialog box of uploading image
                            pd.show();

                            //getting firebase reference
                            mAuth = FirebaseAuth.getInstance();

                            //it gets the phone number with which we have registered on previous screen and save the image with name number.jpg in storage
                            String name = mAuth.getCurrentUser().getPhoneNumber();
                            StorageReference childRef = storageRef.child(name + ".jpg");

                            //uploading the image
                            UploadTask uploadTask = childRef.putFile(filePath);

                            //if image is successfully uploaded it load the image in image box and adds name to local database
                            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                                {
                                    Toast.makeText(StartingDetails.this, "Upload successful", Toast.LENGTH_SHORT).show();
                                    Bitmap bitmap = ((BitmapDrawable) Im.getDrawable()).getBitmap();
                                    if (!db.getMyDetails())
                                    {
                                        //adding details to local db
                                        db.addMyDetails(Status.getText().toString(), bitmap);
                                        addstatusFirebase(Status.getText().toString());
                                    }

                                    //when everything is done it goes to next screen
                                    pd.dismiss();
                                    Intent i = new Intent(getApplicationContext(), MainScreen.class);
                                    startActivity(i);

                                    //on failure it just shows warning
                                }
                                }).addOnFailureListener(new OnFailureListener()
                            {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        pd.dismiss();
                                        Toast.makeText(StartingDetails.this, "Upload Failed -> " + e, Toast.LENGTH_SHORT).show();
                                    }
                            });
                            }
                        else
                        {
                            Toast.makeText(StartingDetails.this, "Select an image", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if ("abc".equals((String) Im.getTag()))
                    {
                        Toast.makeText(StartingDetails.this, "Please select a picture.", Toast.LENGTH_LONG).show();
                    }


                }
                else
                {
                    Toast.makeText(v.getContext(), "Internet not available.", Toast.LENGTH_LONG).show();
                }

            }

        });
    }


    public void addstatusFirebase(final String st)
    {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        //geeting user database on firebase
        DatabaseReference users=rootRef.child("Users");

        //entering new values
        users.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot)
            {
                // Result will be holded Here
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {
                    String phonenum = dsp.child("number").getValue().toString();

                    //adding default status to firebase
                    if(phonenum.equals(mAuth.getCurrentUser().getPhoneNumber()))
                    {
                        dsp.child("status").getRef().setValue("Hello! I'm using chatter");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //nothing to do
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
                else
                    {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            filePath = data.getData();

            try
            {
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                //Setting image to ImageView
                Im.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                Im.setTag("xyz");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //function to check internet availability
    public boolean checkNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            return true;
        }
        else
            return false;
    }
}
