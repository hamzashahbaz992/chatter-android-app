package tech.chatter.com.chatter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static tech.chatter.com.chatter.MainScreen.finalcontacts;

public class UserDetails extends AppCompatActivity {
    String number;
    DbHelper db;
    StorageReference storageRef;
    FirebaseStorage storage;
    ImageView Im;
    TextView Name;
    TextView Status;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Bundle extras = getIntent().getExtras();

        //getting the number of view clicked through intent
        number= extras.getString("number");

        //getting references of imagebox, name and status textbox
        Im = (ImageView)findViewById(R.id.imageView);
        Name = (TextView)findViewById(R.id.textview);
        Status = (TextView)findViewById(R.id.editText2);

        //initializing database
        db = new DbHelper(this);

        //getting current firebase instance
        mAuth=FirebaseAuth.getInstance();

        //getting storage instance
        storage=FirebaseStorage.getInstance();

        //geeting the image name of phone number selected from firebase storage
        String filename=number+".jpg";
        storageRef = storage.getReferenceFromUrl(("gs://chatter-947e6.appspot.com/"+filename));

        //calling function to set name and status textboxes
        getstatusFirebase();

        //loading image
        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(storageRef)
                .into(Im);

    }

    public void getstatusFirebase()
    {
        //getting reference of Users database
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users=rootRef.child("Users");


        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot)
            {
                // Result will be holded Here
                for (com.google.firebase.database.DataSnapshot dsp : dataSnapshot.getChildren())
                {

                    String phonenum = dsp.child("number").getValue().toString();

                    //when phone number selected matches with the one in firebase
                    if(phonenum.equals(number))
                    {
                        //loads status from firebase to status textbox
                        Status.setText(dsp.child("status").getValue().toString());

                        //it iterates in the contact list and when the number matches it loads it name it name textbox
                        for(int j=0;j<finalcontacts.size();j++)
                        {
                            if(finalcontacts.get(j).getNumber().equals(number))
                            {
                                Name.setText(finalcontacts.get(j).getName());
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //do nothing
            }
        });
    }


}
