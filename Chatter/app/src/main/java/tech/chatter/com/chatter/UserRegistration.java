package tech.chatter.com.chatter;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class UserRegistration extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private static final String TAG = "PhoneAuthActivity";
    private Firebase reference;
    String phoneNumber;
    static int pos;
    EditText p;
    Spinner r;
    String[] codes = {"+93","+355","+213","+1684","+376","+956","+217","+91","+315","+92","+971","+44","+1"};;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);

        //getting textview to show code
        p = (EditText) findViewById(R.id.phone);

        //getting countries array and country selected on spinner
        String[] countries = this.getResources().getStringArray(R.array.countries);
        r = (Spinner)findViewById(R.id.spinner);


        //generating code against selected country from codes array
        r.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                pos = position;
                p.setText(codes[position]);
            } // to close the onItemSelected
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        //this code is to get permissions for accessing contacts
        if (ActivityCompat.checkSelfPermission(UserRegistration.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
        {

            ActivityCompat.requestPermissions(UserRegistration.this, new String[]{Manifest.permission.READ_CONTACTS, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2688);

        }
        else
        {

        }



        //this is to save info on application cache so that it can be used anytime without downloading
        try
        {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            FirebaseDatabase.getInstance().getReference("Users");
            FirebaseDatabase.getInstance().getReference("Users").keepSynced(true);
        }
        catch (Exception ex)
        {
            System.out.print(ex);
        }

        //getting textview to show timer and meesage
        final TextView timer=(TextView) findViewById(R.id.textView6);
        timer.setText("Press Next To Get Code");

        mAuth = FirebaseAuth.getInstance();


        //this checks if user exist then it throw us to StartingDetails screen
        if(mAuth.getCurrentUser()!=null)
        {
            Intent I = new Intent(getApplicationContext(), StartingDetails.class);
            startActivity(I);
        }

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.




                //adding new user to Users tree
                Firebase.setAndroidContext(getApplicationContext());
                reference=new Firebase("https://chatter-947e6.firebaseio.com/Users");

                Map<String, String> map = new HashMap<String, String>();
                map.put("number", phoneNumber);
                map.put("status", "Hello! I'm using chatter");
                reference.child(phoneNumber).setValue(map);
                reference.child(phoneNumber).child("OnlineStatus").setValue("");


                signInWithPhoneAuthCredential(credential);


            }

            @Override
            public void onVerificationFailed(FirebaseException e)
            {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.

                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(String verificationId,PhoneAuthProvider.ForceResendingToken token)
            {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

            }
        };

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential)
    {
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            Intent I =new Intent(getApplicationContext(),StartingDetails.class);
                            startActivity(I);

                        }
                        else
                        {
                            // Sign in failed, display a message and update the UI

                            Toast.makeText(getApplicationContext(),"Failed Sign in", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }


    private void verifyPhoneNumberWithCode(String verificationId, String code)
    {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
    }



    public void Start(View v)
    {
        if(checkNetwork(this))
        {
            //getting textview of phone number
            EditText t = (EditText) findViewById(R.id.textView7);

            //concate phone number with code
            phoneNumber = codes[pos] + t.getText().toString();



            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks

            //setting button invisible during timer
            final Button b = (Button) findViewById(R.id.button);
            b.setVisibility(View.INVISIBLE);


            b.postDelayed(new Runnable() {

                public void run() {

                    b.setVisibility(View.VISIBLE);

                }
            }, 60000); // (3000 == 3secs)


            final TextView timer = (TextView) findViewById(R.id.textView6);

            //starting timer

            new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished)
                {
                    timer.setText("Seconds Remaining: " + millisUntilFinished / 1000);
                }

                //After count down ends displays message
                public void onFinish()
                {
                    timer.setText("Press Next To Get Code");
                }
            }.start();
        }
        else
        {
            Toast.makeText(this, "Internet not available.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case 2688:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }


    public boolean checkNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            return true;
        }
        else
            return false;
    }
}
